# Tests

![Where are the tests ?](https://www.commitstrip.com/wp-content/uploads/2017/02/Strip-Ou-sont-les-tests-unitaires-english650-final.jpg "Where are the tests ?")

## Strategy to be defined

Will add some tests soon ;)

### Even if

![Test or not to test, that's the question !](https://www.commitstrip.com/wp-content/uploads/2013/04/Strips-Conscienses-du-test-550-finalenglish.jpg "Test or not to test, that's the question !")

![Tester, c'est douter](https://i2.wp.com/bioinfo-fr.net/wp-content/uploads/2019/07/affiche-tester-c-est-douter-2.jpg "Tester, c'est douter !")
